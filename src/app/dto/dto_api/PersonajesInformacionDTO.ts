import { LocalizacionInformacionDTO } from "./LocalizacionInformacionDTO";
import {  OrigenInformacionDTO } from "./OrigenInformacionDTO";
export class PersonajesInformacionDTO{

    created?:any;
    episode:any;
    gender?:any;
    id?:any;
    image?:any;
    location:LocalizacionInformacionDTO = new LocalizacionInformacionDTO();
    name?:any;
    origin:OrigenInformacionDTO = new OrigenInformacionDTO();
    species?:any;
    status?:any;
    type?:any;
    url?:any;
}