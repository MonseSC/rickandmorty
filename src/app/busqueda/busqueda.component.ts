import { Component, OnInit, ViewChild,ElementRef ,Input} from '@angular/core';
import {LazyLoadEvent} from 'primeng/api';
import { ListaService } from "./busqueda.service";
import { PersonajesInformacionDTO} from "../dto/dto_api/PersonajesInformacionDTO";
import { Table } from 'primeng/table';
import  { PrimeNGConfig} from 'primeng/api';

@Component({
    selector: 'app-busqueda',
    templateUrl: './busqueda.component.html',
    styleUrls: ['./busqueda.component.css']
  })
  
  export class BusquedaComponent implements OnInit {
    @ViewChild('closebuttonDetail') closeButton:any;
    @ViewChild('table') tableSearch:any= Table;
    @Input('ngModel') model: any

    public personajes:Array<any>=[];
    public episodios:any;
    public imagenPersonaje :any;
    public response: PersonajesInformacionDTO= new PersonajesInformacionDTO();
    public cols:any=[];
    public buscar:any;
    public gender:any;
    public value:any;

    public testPrueba:any;
    $:any;

    constructor(public service : ListaService,public nativeElement:ElementRef, private config:PrimeNGConfig) { }
  
    ngOnInit(): void {
     this.obtenerListaPersonajes();
     this.columns();
    }

    obtenerListaPersonajes():any{
        this.service.getList().subscribe(data =>{
            this.personajes= data.results;
        },(err)=>{
            console.error(err);
        }); 
    }

    detailInformation(informacion:any):void{
    this.response = informacion;
    this.episodios = this.response.episode;
    this.showModal();
    }
    columns():void{
     this.cols = [
         {field:'name', header:'Personaje'},
         {field:'gender',header:'Caracteristicas'}
     ];

     this.gender= [
        {label:'Male',value:'Male'}
     ]
    }
    showModal():void{
    const modal = document.getElementById('modalInformation') as HTMLInputElement;
          modal.style.display= 'block';
    }
    
    closeModal():void{
        const modal = document.getElementById('modalInformation') as HTMLInputElement;
              modal.style.display= 'none';
    }

    buscarEvento():void{
        const busqueda = document.getElementById('searchEvent') as HTMLInputElement;
               this.tableSearch.filterGlobal(busqueda.value ,'contains');
    }
    busquedaGender():void{
        const  select2 = document.querySelector('select');
               this.tableSearch.filterGlobal(select2?.value ,'contains');
    }
  }