import { NgModule } from "@angular/core";
import { BusquedaComponent } from "./busqueda.component";
import { TableModule} from "primeng/table";
import { InputTextModule } from "primeng/inputtext";
import {KeyFilterModule as RegexModule,KeyFilterModule} from 'primeng/keyfilter';

@NgModule({

    declarations:[
        BusquedaComponent
    ],
    imports:[
        TableModule,
        InputTextModule,
        KeyFilterModule,
        RegexModule

    ],
    providers:[]

})
export class BusquedaModule{}