import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Constants } from "../constants/constants";
import { Observable } from "rxjs";

@Injectable({
        providedIn:"root"
})

export class ListaService{
constructor(private http:HttpClient){}

/**
 * OBTIENE LA LISTA DEL SERVICIO
 */

getList():Observable<any>{
 return this.http.get(Constants.URL);
}

}